---
title: Al-Fatihah
date: 2021-10-26T07:51:43.911Z
description: AL-Fatihah
---


**Surat al-Fatihah** 

### بِسْمِ ٱللَّهِ ٱلرَّحْمَٰنِ ٱلرَّحِيمِ

`bismillāhir-raḥmānir-raḥīm`

1. Dengan menyebut nama Allah Yang Maha Pemurah lagi Maha Penyayang. 

### ٱلْحَمْدُ لِلَّهِ رَبِّ ٱلْعَٰلَمِينَ

 `al-ḥamdu lillāhi rabbil-‘ālamīn`

2. Segala puji bagi Allah, Tuhan semesta alam

### رَّحْمَٰنِ ٱلرَّحِيمِ

`ar-raḥmānir-raḥīm`

3. Maha Pemurah lagi Maha Penyayang.

### مَٰلِكِ يَوْمِ ٱلدِّينِ

`māliki yaumid-dīn`

4. Yang menguasai di Hari Pembalasan. 

### إِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ

`iyyāka na’budu wa iyyāka nasta’īn`

5. Hanya Engkaulah yang kami sembah, dan hanya kepada Engkaulah kami meminta pertolongan. 

### ٱهْدِنَا ٱلصِّرَٰطَ ٱلْمُسْتَقِيمَ

 ihdinaṣ-ṣirāṭal-mustaqīm

`6. Tunjukilah kami jalan yang lurus`

### صِرَٰطَ ٱلَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ ٱلْمَغْضُوبِ عَلَيْهِمْ وَلَا ٱلضَّآلِّينَ

`ṣirāṭallażīna an’amta ‘alaihim gairil-magḍụbi ‘alaihim wa laḍ-ḍāllīn`

7. (yaitu) Jalan orang-orang yang telah Engkau beri nikmat kepada mereka; bukan (jalan) mereka yang dimurkai dan bukan (pula jalan).